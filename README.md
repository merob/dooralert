# dooralert
https://thepihut.com/products/buzzer-5v-breadboard-friendly

## Materials

### Required
* [Raspberry Pi Zero](https://www.raspberrypi.com/products/raspberry-pi-zero-2-w/) - maybe get a pre-soldered headers unless you really like soldering
* [SD Card](https://www.amazon.co.uk/gp/product/B06XFSZGCC) doesn't need to be big. 32G seems to be the sweet spot on price at the moment
* [Active Buzzer](https://www.amazon.co.uk/gp/product/B098HY5T4N)
* [Magnetic Door Sensor](https://www.amazon.co.uk/gp/product/B07Z4NCWDD)
* [Cable](https://www.amazon.co.uk/gp/product/B094JGXHTJ)
* Soldering iron and solder

### Optional
* [Header kit](https://www.amazon.co.uk/gp/product/B0828SRBTC)
* [Pi Zero case](https://www.amazon.co.uk/gp/product/B08837L144)
* [Heat Shrink Tubing](https://www.amazon.co.uk/gp/product/B092VWZN4W)
* [Wire Connectors](https://www.amazon.co.uk/gp/product/B07KK2BJQV)
* [Drill Bits](https://www.amazon.co.uk/gp/product/B01HY36O5U)
* [Drill Chuck Adapter](https://www.amazon.co.uk/gp/product/B08W3M7X5K)
* [Push Button Switch](https://www.amazon.co.uk/gp/product/B00TXNRBW0)


## Circuit diagram

Created [here](https://www.circuit-diagram.org/editor/)
![board](circuit.png)
