#!/usr/bin/env python3
import sys, time
import traceback
import RPi.GPIO as gpio
import yaml
import threading
import daemon
import datetime

#TODO 
# use max(count) for buzz type
# catch killall signal (same way as keyboard interrupt)
# for button check - catch rising edge instead of polling?
# systemd logging?

class doorAlert:

    class config:
        enabled = True
        debug = False
        buzzerDelay = 30
        doorInputs = [19,13]
        button = 5
        buzzer = 26
        highRail = 0
        buzz = "off" #on off fast slow blip
        cycleTime = 1
        cyclesBeforeBlip = 10
        cyclesBeforeSlow = 30
        cyclesBeforeFast = 45
        unmuteTime = datetime.datetime.now()
        finish = False

    def __init__(self,test=False):
        if test:
            print("TESTING")
        doorAlert.updateConfig(first=True)

        gpio.setmode(gpio.BCM)
        #init door inputs
        for doorInput in getattr(doorAlert.config, "doorInputs"):
            gpio.setup(doorInput, gpio.IN, pull_up_down=gpio.PUD_DOWN)
        #init power input
        gpio.setup(getattr(doorAlert.config, "button"), gpio.IN, pull_up_down=gpio.PUD_DOWN)

        #init buzzer
        gpio.setup(getattr(doorAlert.config, "buzzer"), gpio.OUT)
        gpio.output(getattr(doorAlert.config, "buzzer"), gpio.LOW)

        #init highRail for sensors
        gpio.setup(getattr(doorAlert.config, "highRail"), gpio.OUT)
        gpio.output(getattr(doorAlert.config, "highRail"), gpio.HIGH)

        try:
            buzzerTh = threading.Thread(target=doorAlert.buzzer)
            buzzerTh.start()
            if test:
                testing = threading.Thread(target=doorAlert.testing)
                testing.start()
            else:
                doorAlert.multiBlip(2)
                
                doorCheckTh = threading.Thread(target=doorAlert.doorCheck)
                doorCheckTh.start()
            doorAlert.checkButton()
        except KeyboardInterrupt:
            print("Keyboard interrupt. Exiting main")
        except Exception as e:
           traceback.print_exception(*sys.exc_info())
           print("Error:" + str(e))
        finally:
            print("*******  Cleaning up. Please wait a second  *******")
            setattr(doorAlert.config,"finish", True)
            gpio.output(getattr(doorAlert.config, "buzzer"), gpio.LOW)
            gpio.cleanup()

    def updateConfig(first=False):
        changeOK=["enabled", "debug", "cycleTime", "cyclesBeforeBlip", "cyclesBeforeSlow", "cyclesBeforeFast", "finish"]
        with open('/opt/dooralert/config.yaml') as file:
            try:
                config = yaml.safe_load(file)
                for key, value in config.items():
                    if(getattr(doorAlert.config, key) != value) and (first or key in changeOK):
                        setattr(doorAlert.config, key, value)
            except yaml.YAMLError as exception:
                print(exception)

    def getSensor(pin):
        if not getattr(doorAlert.config, "finish"):
            return gpio.input(pin)
        else:
            return 0

    def doorCheck():
        try:
            openDict = { i : 0 for i in getattr(doorAlert.config, "doorInputs") }
            while True:
                cycleTime = getattr(doorAlert.config, "cycleTime")
                cyclesBeforeBlip = getattr(doorAlert.config, "cyclesBeforeBlip")
                cyclesBeforeSlow = getattr(doorAlert.config, "cyclesBeforeSlow")
                cyclesBeforeFast = getattr(doorAlert.config, "cyclesBeforeFast")
                debug=getattr(doorAlert.config, "debug")

                for doorInput in getattr(doorAlert.config, "doorInputs"):

                    if not doorAlert.getSensor(doorInput):
                        openDict[doorInput] += 1
                        if openDict[doorInput] % cyclesBeforeBlip == 0 and openDict[doorInput] < cyclesBeforeSlow:
                            setattr(doorAlert.config, "buzz", "blip")
                            if debug: print("set blip")
                        elif openDict[doorInput] == cyclesBeforeSlow:
                            setattr(doorAlert.config, "buzz", "slow")
                            if debug: print("set slow")
                        elif openDict[doorInput] == cyclesBeforeFast:
                            setattr(doorAlert.config, "buzz", "fast")
                            if debug: print("set fast")
                    else:
                        openDict[doorInput] = 0
                        if not any(openDict.values()): #only if all doors are closed
                            setattr(doorAlert.config, "buzz", "off")
                            if debug: print("set off")

                if getattr(doorAlert.config, "finish"):
                    sys.exit()
                time.sleep(cycleTime)
                doorAlert.updateConfig()
        except KeyboardInterrupt:
            print("Keyboard interrupt. Exiting DoorCheck")
        except Exception as e:
           traceback.print_exception(*sys.exc_info())
           print("Error:" + str(e))
        finally:
            sys.exit()

    def testing():
        for method in ['on', 'blip', 'fast', 'slow', 'off']:
            if getattr(doorAlert.config, "finish"):
                sys.exit()
            print(method)
            setattr(doorAlert.config, "buzz", method)
            time.sleep(2)
            for doorInput in getattr(doorAlert.config, "doorInputs"):
                if not doorAlert.getSensor(doorInput):
                    print("door sensor " + str(doorInput) + " open")
                else:
                    print("door sensor " + str(doorInput) + " closed")
        setattr(doorAlert.config,"finish", True)
        sys.exit()

    def multiBlip(count):
        for _ in range(count):
            doorAlert.buzzSwitch(1)
            time.sleep(0.1)
            doorAlert.buzzSwitch(0)
            time.sleep(0.1)

    def buzzSwitch(on=0):
        if not getattr(doorAlert.config, "finish"):
            if getattr(doorAlert.config, "enabled"):
                gpio.output(getattr(doorAlert.config, "buzzer"), gpio.HIGH if on else gpio.LOW)
            else:
                print("MUTE MODE: beep", "on" if on else "off")
                time.sleep(0.01)

    def checkButton():
        pin=getattr(doorAlert.config, "button")
        speed=0.1
        offat=5
        count = 0
        while True:
            if doorAlert.getSensor(pin):
                count += 1
                if count == offat:
                    setattr(doorAlert.config, "unmuteTime", datetime.datetime.now() + datetime.timedelta(hours=1))
                    doorAlert.multiBlip(1)
                elif count == offat*2:
                    setattr(doorAlert.config, "unmuteTime", datetime.datetime.now() + datetime.timedelta(hours=4))
                    doorAlert.multiBlip(2)
                elif count == offat*3:
                    setattr(doorAlert.config, "unmuteTime", datetime.datetime.now() + datetime.timedelta(hours=8))
                    doorAlert.multiBlip(3)
                elif count == offat*4:
                    setattr(doorAlert.config, "unmuteTime", datetime.datetime.now() + datetime.timedelta(hours=0))
                    doorAlert.multiBlip(4)
            else:
                count = 0
            time.sleep(speed)

    def buzzer():
        i=0
        on = False
        speed=0.1
        slow=15
        slow_off=3 #off for slow_off*[lenth on]
        fast=5
        blip=2
        count=0
        method="none"
        while True:
            if getattr(doorAlert.config, "unmuteTime") >= datetime.datetime.now():
                if on:
                    doorAlert.buzzSwitch(0)
                    on = False
            elif getattr(doorAlert.config, "buzz") == "on":
                if not on:
                    doorAlert.buzzSwitch(1)
                    on = True
            elif getattr(doorAlert.config, "buzz") == "off":
                if on:
                    doorAlert.buzzSwitch(0)
                    on = False
            elif getattr(doorAlert.config, "buzz") == "slow":
                if method != "slow":
                    method = "slow"
                    count = 0
                else:
                    count += 1
                #if count % slow == 0:
                if count % int(slow/slow_off) == 0:
                    if on:
                        doorAlert.buzzSwitch(0)
                        on = False
                    elif count % slow == 0:
                        doorAlert.buzzSwitch(1)
                        on = True
            elif getattr(doorAlert.config, "buzz") == "fast":
                if method != "fast":
                    method = "fast"
                    count = 0
                else:
                    count += 1
                if count % fast == 0:
                    if on:
                        doorAlert.buzzSwitch(0)
                        on = False
                    else:
                        doorAlert.buzzSwitch(1)
                        on = True
            elif getattr(doorAlert.config, "buzz") == "blip":
                if method != "blip":
                    method = "blip"
                    count = 0
                else:
                    count += 1
                if count % blip == 0:
                    if on:
                        doorAlert.buzzSwitch(0)
                        on = False
                    else:
                        doorAlert.buzzSwitch(1)
                        on = True
                elif count % blip == 1:
                    on = False
                    doorAlert.buzzSwitch(0)
                    setattr(doorAlert.config, "buzz", "off")
                
            i+=1
            if getattr(doorAlert.config, "finish"):
                print("Buzzer exiting")
                sys.exit()
            time.sleep(speed)

if __name__ == '__main__':
    with daemon.DaemonContext():
        doorAlert()





